var DlcSlider = DlcSlider || {};

(function() {

    "use strict";

    var Helper = {

        transitions: [
            'transition',
            'webkitTransition',
            'MozTransition',
            'OTransition'
        ],

        setTransitionStyleOnElement: function(element, value) {
            element.style.transition = value;
            element.style.webkitTransition = value;
            element.style.MozTransition = value;
            element.style.OTransition = value;
        },

        checkSupportTransition: function(element) {
            for(var i = 0, lh = this.transitions.length; i < lh; i++) {
                if(element.style[this.transitions[i]] !== undefined) {
                    return true;
                }
            }
            return false;
        }

    };

    /*
     Обязательные свойства объекта settings
     name - id блока слайдера
     sliderChangeSpeed - скорость передвижения слайдов

     Необязательные свойства объекта settings
     startPosition = 0 - номер первоначально отображаемого слайда
     autoPlay = false - автопрокрутка слайдов
     если true, то обязательный параметр sliderSpeed - скорость перемещения слайдов
     autoHeight = false - если true, то высота слайдера будет настроена так, чтобы полностью был виден по-высоте первый слайд
     если высота остальных слайдов будет больше, будет отображаться только верхняя часть изображения данных слайдов
     Данный параметр обязателен, если в css явно не задана высота слайдера

     animatedControl = true - указывает, будут ли анимированы навигация и пагинация (по умолчанию при выводе курсора из области слайдера они уезжают)
     nav - настройки навигации и пагинации, если они отличаются от заданных по-умолчанию
     baseSize - параметр базового соотношения высоты и ширины слайдера
     */
    var DlcSliderElem = {

        status: 'active',

        statusPag: 'active-pag',

        sliderName: 'dlc-slider',

        animControlSettings: {
            left: '4%',
            right: '4%',
            pag: '5%',
            controlHide: '-1000px',
        },

        constructor: function(settings)
        {

            this.sliderBlock = settings.elem;
            this.sliderContainer = this.sliderBlock.getElementsByClassName(this.sliderName + '-container')[0];
            this.slsBlock = this.sliderBlock.querySelectorAll('.' + this.sliderName + '-container > div')[0];
            this.sliders = this.slsBlock.getElementsByClassName(this.sliderName + '-item');

            this.countSlides = this.sliders.length;
            this.curPos = settings.startPosition;
            this.curPos = (this.curPos && this.curPos > 0 && this.curPos < this.countSlides) ? this.curPos : 0;

            if(settings.autoHeight) {
                this.createAutoHeight();
            }
            else if(settings.baseSize) {
                this.createResize(settings.baseSize);
            }

            this.anim = false;

            this.sliderChangeSpeed = settings.sliderChangeSpeed;
            this.createDelay();
            if(settings.autoPlay === true) {
                this.autoPlay = settings.autoPlay;
                this.sliderSpeed = settings.sliderSpeed;
            }
            if(settings.nav) {
                this.changeControlSettings(settings.nav);
            }

            //устанавливаем текущему слайду атрибут data-status
            this.sliders[this.curPos].setAttribute('data-status', this.status);

            //далее пагинация и навигация
            this.controlElements = Object.create(ControlElements).constructor({
                left: this.sliderBlock.getElementsByClassName(this.sliderName + '-left'),
                right: this.sliderBlock.getElementsByClassName(this.sliderName + '-right'),
                pagination: this.sliderBlock.getElementsByClassName( this.sliderName + '-pagination')[0],
                pags: this.sliderBlock.querySelectorAll('.' + this.sliderName + '-pagination > div'),
            });
            if(this.controlElements.isPagination()) {
                this.controlElements.setPagPos(this.curPos, null, this.statusPag);
            }

            this.createDimensions();
            this.events(settings.animatedControl);

            this.supportTransition = Helper.checkSupportTransition(this.sliderContainer);

            return this;
        },

        //необходимо для правильной работы transition
        createDelay: function() {
            if(this.sliderChangeSpeed < 200) {
                this.timeDelay = 20;
            }
            else if(this.sliderChangeSpeed < 500) {
                this.timeDelay = 40;
            }
            else if(this.sliderChangeSpeed < 2000) {
                this.timeDelay = 60;
            }
            else {
                this.timeDelay = 0.05 * this.sliderChangeSpeed;
            }
        },

        //срабатывает при начале движения слайда
        move: function(event, left)
        {
            event.stopPropagation();
            this.direction = left;
            if(!this.anim) {
                this.anim = true;
                var that = this,
                    width = parseFloat(this.sliders[0].offsetWidth);
                if(left) {
                    width = -width;
                }
                this.sliders[this.curPos].removeAttribute('data-status');
                if((left && this.curPos == 0) || (!left && this.curPos == (this.countSlides-1))) {
                    this.slsBlock.style.left = parseFloat(this.slsBlock.style.left) + width + 'px';
                    Helper.setTransitionStyleOnElement(this.slsBlock, "none");
                    left ?
                        this.slsBlock.insertBefore(this.sliders[this.sliders.length-1], this.sliders[0])
                        : this.slsBlock.appendChild(this.sliders[0]);
                    setTimeout(function() {
                        var sliderTime = that.sliderChangeSpeed - that.timeDelay;
                        that.slsBlock.style.left = (parseInt(that.slsBlock.style.left) - width) + "px";
                        if(!that.supportTransition) {
                            that.endMove();
                        }
                        else {
                            Helper.setTransitionStyleOnElement(that.slsBlock, "left " + sliderTime / 1000 + "s linear");
                            setTimeout(function () {
                                that.endMove();
                            }, that.sliderChangeSpeed - that.timeDelay);
                        }
                    }, that.timeDelay);
                }
                else {
                    this.slsBlock.style.left = (parseInt(this.slsBlock.style.left) - width) + "px";
                    if(!this.supportTransition) {
                        that.endMove();
                    }
                    else {
                        Helper.setTransitionStyleOnElement(this.slsBlock, "left " + this.sliderChangeSpeed / 1000 + "s linear");
                        setTimeout(function () {
                            that.endMove();
                        }, that.sliderChangeSpeed);
                    }
                }
            }
        },

        //срабатывает при окончании движения слайда
        endMove: function()
        {
            var oldPos = this.curPos,
                left = this.direction;
            if(left && this.curPos == 0) {
                this.slsBlock.appendChild(this.sliders[0]);
                this.slsBlock.style.left = -(parseFloat(this.sliders[0].offsetWidth) * (this.countSlides-1)) + 'px';
                Helper.setTransitionStyleOnElement(this.slsBlock, "none");
                this.curPos = this.countSlides;
            }
            else if(!left && this.curPos == (this.countSlides-1)) {
                this.slsBlock.insertBefore(this.sliders[this.countSlides-1], this.sliders[0]);
                this.slsBlock.style.left = 0;
                Helper.setTransitionStyleOnElement(this.slsBlock, "none");
                this.curPos = -1;
            }
            left ? this.curPos-- : this.curPos++;
            this.anim = false;
            this.sliders[this.curPos].setAttribute('data-status', this.status);
            if(this.controlElements.isPagination()) {
                this.controlElements.setPagPos(this.curPos, oldPos, this.statusPag);
            }
        },

        //срабатывает при пролистовании слайдов через пагинацию
        slide: function(event, element)
        {
            event.stopPropagation();
            if(!this.anim) {
                this.anim = true;
                var that=this,
                    offset,
                    newN = parseInt(element.getAttribute('data-number'));
                if(newN != (this.curPos+1)) {
                    this.sliders[this.curPos].removeAttribute('data-status');
                    offset = parseFloat(this.sliders[0].offsetWidth) * (this.curPos+1 - newN);
                    var oldPos = this.curPos;
                    this.curPos = newN-1;
                    this.slsBlock.style.left = parseInt(this.slsBlock.style.left) + offset + "px";
                    Helper.setTransitionStyleOnElement(this.slsBlock, "left " + this.sliderChangeSpeed / 1000 + "s linear");
                    this.sliders[this.curPos].setAttribute('data-status', this.status);
                    if(this.controlElements.isPagination()) {
                        this.controlElements.setPagPos(this.curPos, oldPos, this.statusPag);
                    }
                }
            }
            this.anim = false;
        },

        //в случае авто пролистывания слайдов
        createAutoPlay: function(element)
        {
            clearInterval(this.interval);
            this.interval = setInterval( function() {
                element.click();
            }, (this.sliderSpeed + this.sliderChangeSpeed));
        },

        pauseAutoPlay: function()
        {
            clearInterval(this.interval);
        },

        //настраивает слайдер в соответствии с начальной позицией
        createDimensions: function()
        {
            var lht = this.countSlides;
            this.slsBlock.style.cssText = "width:" + 100*lht + "%; left: " + (-(parseFloat(this.sliders[0].offsetWidth) * this.curPos)) + 'px';
            for(var i=0; i < this.countSlides; i++){
                this.sliders[i].style.width = 100/lht + "%";
            }
        },

        //в случае, если необходима автоматическая настройка высоты
        createAutoHeight: function()
        {
            var im = new Image(),
                that = this;
            im.onload = function() {
                that.sliderContainer.style.height = im.height * that.sliders[0].offsetWidth/im.width + 'px';
                var sizes = window.getComputedStyle(that.sliderBlock, null);
                that.sliderBlock.style.height = parseFloat(sizes.getPropertyValue("height")) + 'px';
                that.sliderContainer.removeAttribute("style");
                that.createResize({width: parseFloat(sizes.getPropertyValue("width")), height: parseFloat(sizes.getPropertyValue("height"))});
            };
            im.src = this.sliders[0].querySelector('img').getAttribute('src');
        },

        createResize: function(baseSizes) {
            var that = this,
                canNotResize = false;
            window.addEventListener('resize', function() {
                that.sliderBlock.style.height = baseSizes.height * parseFloat(window.getComputedStyle(that.sliderBlock, null).getPropertyValue("width"))/baseSizes.width + 'px';
                if(!that.anim) {
                    that.slsBlock.style.left = -(parseFloat(that.sliders[0].offsetWidth) * that.curPos) + 'px';
                }
            }, false);
        },

        events: function(animCtl)
        {
            var left = this.sliderBlock.getElementsByClassName(this.sliderName + '-left'),
                right = this.sliderBlock.getElementsByClassName(this.sliderName + '-right');
            this.setEventHandlers(left, right);
            var animatedControl = (animCtl !== undefined) ? animCtl : true;
            if(animatedControl) {
                this.controlElements.animate(this);
            }
        },

        changeControlSettings: function(settings)
        {
            if(settings.left) {
                this.animControlSettings.left = settings.left;
            }
            if(settings.right) {
                this.animControlSettings.right = settings.right;
            }
            if(settings.pag) {
                this.animControlSettings.pag = settings.pag;
            }
        },

        setEventHandlers: function(left, right)
        {
            var that = this,
                movingElements = this.slsBlock.querySelectorAll('.' + this.sliderName + '-item'),
                i, lh;
            for(i = 0, lh = movingElements.length; i < lh; i++) {
                movingElements[i].addEventListener("click", function(event) {
                    if(this.getAttribute('data-status') === that.status) {
                        that.move(event);
                    }
                }, false);
            }
            if(this.autoPlay) {
                this.createAutoPlay(this.controlElements.right[0]);
                this.sliderBlock.addEventListener('mouseover',
                    function() {
                        that.pauseAutoPlay();
                    }, false);
                this.sliderBlock.addEventListener('mouseleave',
                    function() {
                        that.createAutoPlay(that.controlElements.right[0]);
                    }, false);
            }
            this.controlElements.setEvents(this);
        },

    };

    var ControlElements = {

        constructor: function(settings) {
            this.left = settings.left;
            this.right = settings.right;
            this.pagination = settings.pagination;
            this.pags = settings.pags;
            return this;
        },

        setEvents: function(parentSlider) {
            var i, lh;
            for(i = 0, lh = this.right.length; i < lh; i++) {
                this.right[i].addEventListener("click", function(event) {
                    parentSlider.move(event);
                }, false);
            }
            for(i = 0, lh = this.left.length; i < lh; i++) {
                this.left[i].addEventListener("click", function(event) {
                    parentSlider.move(event, true);
                }, false);
            }
            for(i = 0, lh = this.pags.length; i < lh; i++) {
                this.pags[i].addEventListener("click", function(event) {
                    parentSlider.slide(event, this);
                }, false);
            }
        },

        isPagination: function() {
            return this.pagination ? true : false;
        },

        setPagPos: function(pos, oldPos, className)
        {
            if(oldPos !== undefined && oldPos !== null) {
                this.pags[oldPos].removeAttribute('class');
            }
            this.pags[pos].className = className;
        },

        animate: function(parentSlider) {
            var left = this.left[0],
                right = this.right[0],
                pagination = this.pagination,
                lt = parentSlider.animControlSettings.left,
                rt = parentSlider.animControlSettings.right,
                pag = parentSlider.animControlSettings.pag,
                ch = parentSlider.animControlSettings.controlHide,
                i, lh, timer;
            left.style.cssText = "left:" + ch + ";display:none;";
            right.style.cssText = "right:" + ch + ";display:none";
            Helper.setTransitionStyleOnElement(left, "left 0.5s linear");
            Helper.setTransitionStyleOnElement(right, "right 0.5s linear");
            if(pagination) {
                pagination.style.cssText = "bottom:" + ch + ";display:none";
                Helper.setTransitionStyleOnElement(pagination, "bottom 0.5s linear");
            }

            //для того, чтобы появились переключатели
            parentSlider.sliderBlock.addEventListener('mouseover', function() {
                if(timer) {
                    clearTimeout(timer);
                }
                left.style.display = 'block';
                left.style.left = lt;
                right.style.display = 'block';
                right.style.right = rt;
                if(pagination) {
                    pagination.style.display = 'block';
                    pagination.style.bottom = pag;
                }
            }, false);
            parentSlider.sliderBlock.addEventListener('mouseleave', function() {
                left.style.left = ch;
                right.style.right = ch;
                pagination.style.bottom = ch;
                timer = setTimeout(function() {
                    left.style.display = "none";
                    right.style.display = "none";
                    pagination.style.display = "none";
                }, 500);
            }, false);
        },

    };

    DlcSlider.create = function(settings) {
        settings.elem = document.getElementById(settings.sliderBlockId);
        Object.create(DlcSliderElem).constructor(settings);
    };

}());
