(function($) {
	/*
		Обязательные свойства объекта settings
			name - id блока слайдера
			sliderChangeSpeed - скорость передвижения слайдов
		
		Необязательные свойства объекта settings
			startPosition = 0 - номер первоначально отображаемого слайда
			autoPlay = false - автопрокрутка слайдов
				если true, то обязательный параметр sliderSpeed - скорость перемещения слайдов
			autoHeight = false - если true, то высота слайдера будет настроена так, чтобы полностью был виден по-высоте первый слайд
									если высота остальных слайдов будет больше, будет отображаться только верхняя часть изображения данных слайдов
								Данный параметр обязателен, если в css явно не задана высота слайдера
		
			animatedControl = true - указывает, будут ли анимированы навигация и пагинация (по умолчанию при выводе курсора из области слайдера они уезжают)
			nav - настройки навигации и пагинации, если они отличаются от заданных по-умолчанию
			baseSize - параметр базового соотношения высоты и ширины слайдера
	*/
	var DlcSlider = {
	
		status: 'active',
		
		statusPag: 'active-pag',
		
		sliderName: 'dlc-slider',
		
		animControlSettings: {
			left: '4%',
			right: '4%',
			pag: '5%',
			controlHide: '-1000px',
		},
	
		constructor: function(settings)
		{
			
			this.sliderBlock = settings.elem;
			this.sliderContainer = this.sliderBlock.getElementsByClassName(this.sliderName + '-container')[0];
			this.slsBlock = this.sliderBlock.querySelectorAll('.' + this.sliderName + '-container > div')[0];
			this.sliders = this.slsBlock.getElementsByClassName(this.sliderName + '-item');
			this.pag = this.sliderBlock.querySelectorAll('.' + this.sliderName + '-pagination > div');
			
			this.countSlides = this.sliders.length;
			this.curPos = settings.startPosition;
			this.curPos = (this.curPos && this.curPos > 0 && this.curPos < this.countSlides) ? this.curPos : 0;
			
			if(settings.autoHeight) {
				this.createAutoHeight();
			}
			else if(settings.baseSize) {
				this.createResize(settings.baseSize);
			}
			
			this.anim = false;
			
			this.sliderChangeSpeed = settings.sliderChangeSpeed;
			if(settings.autoPlay === true) {
				this.autoPlay = settings.autoPlay;
				this.sliderSpeed = settings.sliderSpeed;
			}
			if(settings.nav) {
				this.changeControlSettings(settings.nav);
			}
			
			//устанавливаем текущему слайду атрибут data-status
			this.sliders[this.curPos].setAttribute('data-status', this.status); 
			if(this.pag.length) {
				this.setPagPos(this.curPos);
			}
			
			this.createDimensions();
			this.events(settings.animatedControl, settings.name);
			
		},
		
		//срабатывает при начале движения слайда
		move: function(event, left)
		{
			event.stopPropagation();
			if(!this.anim) {
				this.anim = true;
				var slider = this,
					width = parseFloat(this.sliders[0].offsetWidth);
				if(left) {
					width = -width;
				}
				this.sliders[this.curPos].removeAttribute('data-status');
				if((left && this.curPos == 0) || (!left && this.curPos == (this.countSlides-1))) {
					this.slsBlock.style.left = parseFloat(this.slsBlock.style.left) + width + 'px';
					left ? this.slsBlock.insertBefore(this.sliders[this.sliders.length-1], this.sliders[0]) : this.slsBlock.appendChild(this.sliders[0]);
				}
				$(this.slsBlock).animate({left:'-=' + width}, this.sliderChangeSpeed, function(){ slider.endMove(left)});
			}
		},
		
		//срабатывает при окончании движения слайда
		endMove: function(left)
		{
			var oldPos = this.curPos;
			if(left && this.curPos == 0) {
				this.slsBlock.appendChild(this.sliders[0]);
				this.slsBlock.style.left = -(parseFloat(this.sliders[0].offsetWidth) * (this.countSlides-1)) + 'px';
				this.curPos = this.countSlides;
			}
			else if(!left && this.curPos == (this.countSlides-1)) {
				this.slsBlock.insertBefore(this.sliders[this.countSlides-1], this.sliders[0]);
				this.slsBlock.style.left = 0;
				this.curPos = -1;
			}
			left ? this.curPos-- : this.curPos++;
			this.anim = false;
			this.sliders[this.curPos].setAttribute('data-status', this.status);
			if(this.pag.length) {
				this.setPagPos(this.curPos, oldPos);
			}
		},
		
		//срабатывает при пролистовании слайдов через пагинацию
		slide: function(event, element)
		{
			event.stopPropagation();
			if(!this.anim) {
				this.anim = true;
				var that=this, 
					offset, 
					newN = parseInt(element.getAttribute('data-number'));
				if(newN != (this.curPos+1)) {
					this.sliders[this.curPos].removeAttribute('data-status');
					offset = parseFloat(this.sliders[0].offsetWidth) * (this.curPos+1 - newN);
					var oldPos = this.curPos;
					this.curPos = newN-1;
					$(this.slsBlock).animate({left:'+=' + offset}, that.sliderChangeSpeed);
					this.sliders[this.curPos].setAttribute('data-status', this.status);
					if(this.pag.length) {
						this.setPagPos(this.curPos, oldPos); //устанавливаем новый активный элемент пагинации
					}
				}
			}
			this.anim = false;
		},
		
		//в случае авто пролистывания слайдов
		createAutoPlay: function($element)
		{
			clearInterval(this.interval);
			this.interval = setInterval(function(){$element.trigger('click');}, (this.sliderSpeed + this.sliderChangeSpeed));
		},
		
		pauseAutoPlay: function()
		{
			clearInterval(this.interval);
		},
		
		setPagPos: function(pos, oldPos)
		{
			if(oldPos !== undefined)
				this.pag[oldPos].removeAttribute('class');
			this.pag[pos].className = this.statusPag;
		},
		
		//настраивает слайдер в соответствии с начальной позицией
		createDimensions: function()
		{
			var lht = this.countSlides;
			this.slsBlock.style.cssText = "width:" + 100*lht + "%; left: " + (-(parseFloat(this.sliders[0].offsetWidth) * this.curPos)) + 'px';
			for(var i=0; i < this.countSlides; i++){
				this.sliders[i].style.width = 100/lht + "%";
			}
		},
		
		//в случае, если необходима автоматическая настройка высоты
		createAutoHeight: function() 
		{
			var im = new Image(), that = this;
			im.onload = function() {
				that.sliderContainer.style.height = im.height * that.sliders[0].offsetWidth/im.width + 'px';
				var sizes = window.getComputedStyle(that.sliderBlock, null);
				that.sliderBlock.style.height = parseFloat(sizes.getPropertyValue("height")) + 'px';
				that.sliderContainer.removeAttribute("style");
				that.createResize({width: parseFloat(sizes.getPropertyValue("width")), height: parseFloat(sizes.getPropertyValue("height"))});
			}
			im.src = this.sliders[0].querySelector('img').getAttribute('src');
		},
		
		createResize: function(baseSizes) {
			var that = this,
				canNotResize = false;
			window.addEventListener('resize', function() {
				if(!canNotResize) {
					that.sliderBlock.style.height = baseSizes.height * parseFloat(window.getComputedStyle(that.sliderBlock, null).getPropertyValue("width"))/baseSizes.width + 'px';
					if(!that.anim) {
						that.slsBlock.style.left = -(parseFloat(that.sliders[0].offsetWidth) * that.curPos) + 'px';
					}
					canNotResize = true;
					setTimeout(function() { canNotResize = false; }, 100);
				}
			}, false);
		},
		
		events: function(animCtl, sliderId)
		{
			var $left = $(this.sliderBlock).find('.' + this.sliderName + '-left'),
				$right = $(this.sliderBlock).find('.' + this.sliderName + '-right');
			this.setEventHandlers($left, $right);
			var animatedControl = (animCtl !== undefined) ? animCtl : true;
			if(animatedControl) {
				this.craeteAnimControl($left, $right, sliderId);
			}
		},
		
		setEventHandlers: function($left, $right)
		{
			var slider = this;
			$(this.slsBlock).on('click', '.' + this.sliderName + '-item[data-status=' + this.status + ']', function(event){ slider.move(event); });
			$right.on('click', function(event){ slider.move(event); });
			$left.on('click', function(event){ slider.move(event, true); });
			$(this.pag).on('click', function(event){slider.slide(event, this)});
			if(this.autoPlay) {
				this.createAutoPlay($right);
				$(this.sliderBlock).on('mouseover', function(){ slider.pauseAutoPlay(); });
				$(this.sliderBlock).on('mouseleave', function(){ slider.createAutoPlay($right); });
			}
		},
		
		craeteAnimControl: function($left, $right, sliderId)
		{
			var $slBl = $(this.sliderBlock),
				$pag = $slBl.find('.' + this.sliderName + '-pagination'),
				lt = this.animControlSettings.left,
				rt = this.animControlSettings.right,
				pag = this.animControlSettings.pag,
				ch = this.animControlSettings.controlHide;
			$left.css({left: ch, display: 'none'});
			$right.css({right: ch, display: 'none'});
			if($pag.length) { 
				$pag.css({bottom: ch, display: 'none'});
			}
			//для того, чтобы появились переключатели
			$slBl.on('mouseover', function(){ 
				$left.css({display: 'block'}).stop().animate( {left: lt}, 500);
				$right.css({display: 'block'}).stop().animate( {right: rt}, 500);
				if($pag.length) {
					$pag.css({display: 'block'}).stop().animate( {bottom: pag}, 500);
				}
			});
			$slBl.on('mouseleave', function(){ 
				$left.stop().animate( {left: ch}, 500, function(){ $(this).css({display: 'none'}); });
				$right.stop().animate( {right: ch}, 500, function(){ $(this).css({display: 'none'}); }); 
				if($pag.length) {
					$pag.stop().animate( {bottom: ch}, 500, function(){ $(this).css({display: 'none'}); });
				}
			});
		},
		
		changeControlSettings: function(settings) 
		{
			if(settings.left) {
				this.animControlSettings.left = settings.left;
			}
			if(settings.right) {
				this.animControlSettings.right = settings.right;
			}
			if(settings.pag) {
				this.animControlSettings.pag = settings.pag;
			}
		},
		
	};
	
	$.fn.DlcSlider = function(options) {
		this.each(function() {
			options.elem = this;
			Object.create(DlcSlider).constructor(options);
		});
	};
	
}(jQuery));